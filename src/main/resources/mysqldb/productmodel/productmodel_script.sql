/*
  name: mysql sample database classicmodels
  link: http://www.mysqltutorial.org/mysql-sample-database.aspx
*/


/* create the database */
create database  if not exists productmodel;

/* switch to the productmodel database */
use productmodel;

/* drop existing tables  */
drop table if exists productline;
drop table if exists product;
drop table if exists office;
drop table if exists employee;
drop table if exists customer; 
drop table if exists payment;
drop table if exists ordernumberdetail;
drop table if exists orderdetail;

/* create the tables */
create table productline (
  productline varchar(50),
  textdescription varchar(4000) default null,
  htmldescription mediumtext,
  image mediumblob,
  primary key (productline)
);

create table product (
  productcode varchar(15),
  productname varchar(70) not null,
  productline varchar(50) not null,
  productcale varchar(10) not null,
  productvendor varchar(50) not null,
  productdescription text not null,
  quantityinstock smallint(6) not null,
  buyprice decimal(10,2) not null,
  msrp decimal(10,2) not null,
  primary key (productcode),
  foreign key (productline) references productline (productline)
);

create table office (
  officecode varchar(10),
  city varchar(50) not null,
  phone varchar(50) not null,
  addressline1 varchar(50) not null,
  addressline2 varchar(50) default null,
  state varchar(50) default null,
  country varchar(50) not null,
  postalcode varchar(15) not null,
  territory varchar(10) not null,
  primary key (officecode)
);

create table employee (
  employeenumber int,
  lastname varchar(50) not null,
  firstname varchar(50) not null,
  extension varchar(10) not null,
  email varchar(100) not null,
  officecode varchar(10) not null,
  reportsto int default null,
  jobtitle varchar(50) not null,
  primary key (employeenumber),
  foreign key (reportsto) references employee (employeenumber),
  foreign key (officecode) references office (officecode)
);

create table customer (
  customernumber int,
  customername varchar(50) not null,
  contactlastname varchar(50) not null,
  contactfirstname varchar(50) not null,
  phone varchar(50) not null,
  addressline1 varchar(50) not null,
  addressline2 varchar(50) default null,
  city varchar(50) not null,
  state varchar(50) default null,
  postalcode varchar(15) default null,
  country varchar(50) not null,
  salesrepemployeenumber int default null,
  creditlimit decimal(10,2) default null,
  primary key (customernumber),
  foreign key (salesrepemployeenumber) references employee (employeenumber)
);

create table payment (
  customernumber int,
  checknumber varchar(50) not null,
  paymentdate date not null,
  amount decimal(10,2) not null,
  primary key (customernumber,checknumber),
  foreign key (customernumber) references customer (customernumber)
);

create table ordernumberdetail (
  ordernumber int,
  orderdate date not null,
  requireddate date not null,
  shippeddate date default null,
  status varchar(15) not null,
  comments text,
  customernumber int not null,
  primary key (ordernumber),
  foreign key (customernumber) references customer (customernumber)
);

create table orderdetail (
  ordernumber int,
  productcode varchar(15) not null,
  quantityordered int not null,
  priceeach decimal(10,2) not null,
  orderlinenumber smallint(6) not null,
  primary key (ordernumber,productcode),
  foreign key (ordernumber) references ordernumberdetail (ordernumber),
  foreign key (productcode) references product (productcode)
);

