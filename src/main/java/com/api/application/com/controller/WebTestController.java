package com.api.application.com.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WebTestController {

	@GetMapping("ping")
	public String ping() {
		return "Up and running";
	}
}
