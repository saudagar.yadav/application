package com.api.application.com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.application.com.entity.Productline;
import com.api.application.com.repo.ProductlineRepository;

@RestController
public class ProductLineController {

	@Autowired
	ProductlineRepository productlineRepository;
	
	@GetMapping("getProductLine")
	public List<Productline> getProductLine(){
		return productlineRepository.findAll();
	}
}
