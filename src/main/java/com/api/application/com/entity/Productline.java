package com.api.application.com.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import java.util.Set;


@Entity
public class Productline {

    @Id
    @Column(nullable = false, updatable = false, length = 50)
    private String productline;

    @Column(length = 4000)
    private String textdescription;

    @Column(columnDefinition = "longtext")
    private String htmldescription;

    @Column(columnDefinition = "longtext")
    private String image;

    @OneToMany(mappedBy = "productline")
    private Set<Product> productlineProducts;

    public String getProductline() {
        return productline;
    }

    public void setProductline(final String productline) {
        this.productline = productline;
    }

    public String getTextdescription() {
        return textdescription;
    }

    public void setTextdescription(final String textdescription) {
        this.textdescription = textdescription;
    }

    public String getHtmldescription() {
        return htmldescription;
    }

    public void setHtmldescription(final String htmldescription) {
        this.htmldescription = htmldescription;
    }

    public String getImage() {
        return image;
    }

    public void setImage(final String image) {
        this.image = image;
    }

    public Set<Product> getProductlineProducts() {
        return productlineProducts;
    }

    public void setProductlineProducts(final Set<Product> productlineProducts) {
        this.productlineProducts = productlineProducts;
    }

}
