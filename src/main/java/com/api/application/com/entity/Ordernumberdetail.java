package com.api.application.com.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import java.time.LocalDate;
import java.util.Set;


@Entity
public class Ordernumberdetail {

    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ordernumber;

    @Column(nullable = false)
    private LocalDate orderdate;

    @Column(nullable = false)
    private LocalDate requireddate;

    @Column
    private LocalDate shippeddate;

    @Column(nullable = false, length = 15)
    private String status;

    @Column(columnDefinition = "longtext")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customernumber", nullable = false)
    private Customer customernumber;

    @OneToMany(mappedBy = "ordernumber")
    private Set<Orderdetail> ordernumberOrderdetails;

    public Integer getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(final Integer ordernumber) {
        this.ordernumber = ordernumber;
    }

    public LocalDate getOrderdate() {
        return orderdate;
    }

    public void setOrderdate(final LocalDate orderdate) {
        this.orderdate = orderdate;
    }

    public LocalDate getRequireddate() {
        return requireddate;
    }

    public void setRequireddate(final LocalDate requireddate) {
        this.requireddate = requireddate;
    }

    public LocalDate getShippeddate() {
        return shippeddate;
    }

    public void setShippeddate(final LocalDate shippeddate) {
        this.shippeddate = shippeddate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(final String comments) {
        this.comments = comments;
    }

    public Customer getCustomernumber() {
        return customernumber;
    }

    public void setCustomernumber(final Customer customernumber) {
        this.customernumber = customernumber;
    }

    public Set<Orderdetail> getOrdernumberOrderdetails() {
        return ordernumberOrderdetails;
    }

    public void setOrdernumberOrderdetails(final Set<Orderdetail> ordernumberOrderdetails) {
        this.ordernumberOrderdetails = ordernumberOrderdetails;
    }

}
