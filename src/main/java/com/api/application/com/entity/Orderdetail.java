package com.api.application.com.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import java.math.BigDecimal;


@Entity
public class Orderdetail {

    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer quantityordered;

    @Column(nullable = false, precision = 12, scale = 2)
    private BigDecimal priceeach;

    @Column(nullable = false)
    private Integer orderlinenumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ordernumber")
    private Ordernumberdetail ordernumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "productcode", nullable = false)
    private Product productcode;

    public Integer getQuantityordered() {
        return quantityordered;
    }

    public void setQuantityordered(final Integer quantityordered) {
        this.quantityordered = quantityordered;
    }

    public BigDecimal getPriceeach() {
        return priceeach;
    }

    public void setPriceeach(final BigDecimal priceeach) {
        this.priceeach = priceeach;
    }

    public Integer getOrderlinenumber() {
        return orderlinenumber;
    }

    public void setOrderlinenumber(final Integer orderlinenumber) {
        this.orderlinenumber = orderlinenumber;
    }

    public Ordernumberdetail getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(final Ordernumberdetail ordernumber) {
        this.ordernumber = ordernumber;
    }

    public Product getProductcode() {
        return productcode;
    }

    public void setProductcode(final Product productcode) {
        this.productcode = productcode;
    }

}
