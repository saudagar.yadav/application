package com.api.application.com.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import java.util.Set;


@Entity
public class Office {

    @Id
    @Column(nullable = false, updatable = false, length = 10)
    private String officecode;

    @Column(nullable = false, length = 50)
    private String city;

    @Column(nullable = false, length = 50)
    private String phone;

    @Column(nullable = false, length = 50)
    private String addressline1;

    @Column(length = 50)
    private String addressline2;

    @Column(length = 50)
    private String state;

    @Column(nullable = false, length = 50)
    private String country;

    @Column(nullable = false, length = 15)
    private String postalcode;

    @Column(nullable = false, length = 10)
    private String territory;

    @OneToMany(mappedBy = "officecode")
    private Set<Employee> officecodeEmployees;

    public String getOfficecode() {
        return officecode;
    }

    public void setOfficecode(final String officecode) {
        this.officecode = officecode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public String getAddressline1() {
        return addressline1;
    }

    public void setAddressline1(final String addressline1) {
        this.addressline1 = addressline1;
    }

    public String getAddressline2() {
        return addressline2;
    }

    public void setAddressline2(final String addressline2) {
        this.addressline2 = addressline2;
    }

    public String getState() {
        return state;
    }

    public void setState(final String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(final String country) {
        this.country = country;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(final String postalcode) {
        this.postalcode = postalcode;
    }

    public String getTerritory() {
        return territory;
    }

    public void setTerritory(final String territory) {
        this.territory = territory;
    }

    public Set<Employee> getOfficecodeEmployees() {
        return officecodeEmployees;
    }

    public void setOfficecodeEmployees(final Set<Employee> officecodeEmployees) {
        this.officecodeEmployees = officecodeEmployees;
    }

}
