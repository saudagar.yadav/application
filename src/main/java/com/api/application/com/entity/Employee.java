package com.api.application.com.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import java.util.Set;


@Entity
public class Employee {

    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer employeenumber;

    @Column(nullable = false, length = 50)
    private String lastname;

    @Column(nullable = false, length = 50)
    private String firstname;

    @Column(nullable = false, length = 10)
    private String extension;

    @Column(nullable = false, length = 100)
    private String email;

    @Column(nullable = false, length = 50)
    private String jobtitle;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reportsto")
    private Employee reportsto;

    @OneToMany(mappedBy = "reportsto")
    private Set<Employee> reportstoEmployees;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "officecode", nullable = false)
    private Office officecode;

    @OneToMany(mappedBy = "salesrepemployeenumber")
    private Set<Customer> salesrepemployeenumberCustomers;

    public Integer getEmployeenumber() {
        return employeenumber;
    }

    public void setEmployeenumber(final Integer employeenumber) {
        this.employeenumber = employeenumber;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(final String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(final String firstname) {
        this.firstname = firstname;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(final String extension) {
        this.extension = extension;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getJobtitle() {
        return jobtitle;
    }

    public void setJobtitle(final String jobtitle) {
        this.jobtitle = jobtitle;
    }

    public Employee getReportsto() {
        return reportsto;
    }

    public void setReportsto(final Employee reportsto) {
        this.reportsto = reportsto;
    }

    public Set<Employee> getReportstoEmployees() {
        return reportstoEmployees;
    }

    public void setReportstoEmployees(final Set<Employee> reportstoEmployees) {
        this.reportstoEmployees = reportstoEmployees;
    }

    public Office getOfficecode() {
        return officecode;
    }

    public void setOfficecode(final Office officecode) {
        this.officecode = officecode;
    }

    public Set<Customer> getSalesrepemployeenumberCustomers() {
        return salesrepemployeenumberCustomers;
    }

    public void setSalesrepemployeenumberCustomers(
            final Set<Customer> salesrepemployeenumberCustomers) {
        this.salesrepemployeenumberCustomers = salesrepemployeenumberCustomers;
    }

}
