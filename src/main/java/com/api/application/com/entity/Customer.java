package com.api.application.com.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import java.math.BigDecimal;
import java.util.Set;


@Entity
public class Customer {

    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer customernumber;

    @Column(nullable = false, length = 50)
    private String customername;

    @Column(nullable = false, length = 50)
    private String contactlastname;

    @Column(nullable = false, length = 50)
    private String contactfirstname;

    @Column(nullable = false, length = 50)
    private String phone;

    @Column(nullable = false, length = 50)
    private String addressline1;

    @Column(length = 50)
    private String addressline2;

    @Column(nullable = false, length = 50)
    private String city;

    @Column(length = 50)
    private String state;

    @Column(length = 15)
    private String postalcode;

    @Column(nullable = false, length = 50)
    private String country;

    @Column(precision = 12, scale = 2)
    private BigDecimal creditlimit;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "salesrepemployeenumber")
    private Employee salesrepemployeenumber;

    @OneToMany(mappedBy = "customernumber")
    private Set<Payment> customernumberPayments;

    @OneToMany(mappedBy = "customernumber")
    private Set<Ordernumberdetail> customernumberOrdernumberdetails;

    public Integer getCustomernumber() {
        return customernumber;
    }

    public void setCustomernumber(final Integer customernumber) {
        this.customernumber = customernumber;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(final String customername) {
        this.customername = customername;
    }

    public String getContactlastname() {
        return contactlastname;
    }

    public void setContactlastname(final String contactlastname) {
        this.contactlastname = contactlastname;
    }

    public String getContactfirstname() {
        return contactfirstname;
    }

    public void setContactfirstname(final String contactfirstname) {
        this.contactfirstname = contactfirstname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public String getAddressline1() {
        return addressline1;
    }

    public void setAddressline1(final String addressline1) {
        this.addressline1 = addressline1;
    }

    public String getAddressline2() {
        return addressline2;
    }

    public void setAddressline2(final String addressline2) {
        this.addressline2 = addressline2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(final String state) {
        this.state = state;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(final String postalcode) {
        this.postalcode = postalcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(final String country) {
        this.country = country;
    }

    public BigDecimal getCreditlimit() {
        return creditlimit;
    }

    public void setCreditlimit(final BigDecimal creditlimit) {
        this.creditlimit = creditlimit;
    }

    public Employee getSalesrepemployeenumber() {
        return salesrepemployeenumber;
    }

    public void setSalesrepemployeenumber(final Employee salesrepemployeenumber) {
        this.salesrepemployeenumber = salesrepemployeenumber;
    }

    public Set<Payment> getCustomernumberPayments() {
        return customernumberPayments;
    }

    public void setCustomernumberPayments(final Set<Payment> customernumberPayments) {
        this.customernumberPayments = customernumberPayments;
    }

    public Set<Ordernumberdetail> getCustomernumberOrdernumberdetails() {
        return customernumberOrdernumberdetails;
    }

    public void setCustomernumberOrdernumberdetails(
            final Set<Ordernumberdetail> customernumberOrdernumberdetails) {
        this.customernumberOrdernumberdetails = customernumberOrdernumberdetails;
    }

}
