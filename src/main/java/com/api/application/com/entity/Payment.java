package com.api.application.com.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import java.math.BigDecimal;
import java.time.LocalDate;


@Entity
public class Payment {

    @Id
    @Column(nullable = false, updatable = false, length = 50)
    private String checknumber;

    @Column(nullable = false)
    private LocalDate paymentdate;

    @Column(nullable = false, precision = 12, scale = 2)
    private BigDecimal amount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customernumber")
    private Customer customernumber;

    public String getChecknumber() {
        return checknumber;
    }

    public void setChecknumber(final String checknumber) {
        this.checknumber = checknumber;
    }

    public LocalDate getPaymentdate() {
        return paymentdate;
    }

    public void setPaymentdate(final LocalDate paymentdate) {
        this.paymentdate = paymentdate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    public Customer getCustomernumber() {
        return customernumber;
    }

    public void setCustomernumber(final Customer customernumber) {
        this.customernumber = customernumber;
    }

}
