package com.api.application.com.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import java.math.BigDecimal;
import java.util.Set;


@Entity
public class Product {

    @Id
    @Column(nullable = false, updatable = false, length = 15)
    private String productcode;

    @Column(nullable = false, length = 70)
    private String productname;

    @Column(nullable = false, length = 10)
    private String productcale;

    @Column(nullable = false, length = 50)
    private String productvendor;

    @Column(nullable = false, columnDefinition = "longtext")
    private String productdescription;

    @Column(nullable = false)
    private Integer quantityinstock;

    @Column(nullable = false, precision = 12, scale = 2)
    private BigDecimal buyprice;

    @Column(nullable = false, precision = 12, scale = 2)
    private BigDecimal msrp;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "productline", nullable = false)
    private Productline productline;

    @OneToMany(mappedBy = "productcode")
    private Set<Orderdetail> productcodeOrderdetails;

    public String getProductcode() {
        return productcode;
    }

    public void setProductcode(final String productcode) {
        this.productcode = productcode;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(final String productname) {
        this.productname = productname;
    }

    public String getProductcale() {
        return productcale;
    }

    public void setProductcale(final String productcale) {
        this.productcale = productcale;
    }

    public String getProductvendor() {
        return productvendor;
    }

    public void setProductvendor(final String productvendor) {
        this.productvendor = productvendor;
    }

    public String getProductdescription() {
        return productdescription;
    }

    public void setProductdescription(final String productdescription) {
        this.productdescription = productdescription;
    }

    public Integer getQuantityinstock() {
        return quantityinstock;
    }

    public void setQuantityinstock(final Integer quantityinstock) {
        this.quantityinstock = quantityinstock;
    }

    public BigDecimal getBuyprice() {
        return buyprice;
    }

    public void setBuyprice(final BigDecimal buyprice) {
        this.buyprice = buyprice;
    }

    public BigDecimal getMsrp() {
        return msrp;
    }

    public void setMsrp(final BigDecimal msrp) {
        this.msrp = msrp;
    }

    public Productline getProductline() {
        return productline;
    }

    public void setProductline(final Productline productline) {
        this.productline = productline;
    }

    public Set<Orderdetail> getProductcodeOrderdetails() {
        return productcodeOrderdetails;
    }

    public void setProductcodeOrderdetails(final Set<Orderdetail> productcodeOrderdetails) {
        this.productcodeOrderdetails = productcodeOrderdetails;
    }

}
