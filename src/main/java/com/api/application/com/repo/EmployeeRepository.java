package com.api.application.com.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.application.com.entity.Employee;


public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
}
