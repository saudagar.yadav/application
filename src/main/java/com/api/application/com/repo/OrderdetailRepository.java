package com.api.application.com.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.application.com.entity.Orderdetail;


public interface OrderdetailRepository extends JpaRepository<Orderdetail, Integer> {
}
