package com.api.application.com.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.application.com.entity.Productline;


public interface ProductlineRepository extends JpaRepository<Productline, String> {
}
