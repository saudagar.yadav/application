package com.api.application.com.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.application.com.entity.Customer;


public interface CustomerRepository extends JpaRepository<Customer, Integer> {
}
