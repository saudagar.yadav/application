package com.api.application.com.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.application.com.entity.Ordernumberdetail;


public interface OrderNumberDetailsRepository extends JpaRepository<Ordernumberdetail, Integer> {
}
