package com.api.application.com.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.application.com.entity.Product;


public interface ProductRepository extends JpaRepository<Product, String> {
}
