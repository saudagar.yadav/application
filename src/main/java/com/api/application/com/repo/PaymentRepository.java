package com.api.application.com.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.application.com.entity.Payment;


public interface PaymentRepository extends JpaRepository<Payment, String> {
}
